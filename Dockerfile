FROM python:3.11-alpine

WORKDIR /

COPY . .



RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 7001

CMD [ "flask", "run","--host","0.0.0.0","--port","7001"]